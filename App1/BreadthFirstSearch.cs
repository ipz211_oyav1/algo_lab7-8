﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class BreadthFirstSearch
    {
        static public void BFS(int v, int[,] graph, bool[] visited, string[] city)
        {
            int[] path = new int[graph.GetLength(0)]; //масив з довжиною шляху для кожноъ вершини
            string start = city[v]; // запам'ятовуємо початкове місто
            Queue<int> q = new Queue<int>(); // черга для міст
            visited[v] = true; // помічаємо місто, як відвідане
            q.Enqueue(v); // заносимо це місто у чергу
            while (q.Count != 0) // якщо черга не пуста
            {
                v = q.Dequeue(); // вилучаємо елемент
                for (int i = 0; i < graph.GetLength(0); i++) // проходимо всі вершини
                {
                    if (graph[v, i] != 0 && visited[i] != true) // якщо у вершини є не відвідані зв'язані вершини
                    {
                        visited[i] = true; // помічаємо наступну вершину, як відвідану
                        q.Enqueue(i); // додаємо вершину в чергу
                        path[i] = path[v] + graph[v, i]; // рахуємо пройдений шлях до цієї вершини
                        Console.WriteLine(start + " - " + city[i] + ": " + path[i] + "км");
                    }
                }
            }

        }
    }
}