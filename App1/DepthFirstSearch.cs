﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class DepthFirstSearch
    {
        public static void DFS(int v, int[,] graph, bool[] visited, string[] city, int km = 0, string start ="")
        {
            if (start == "")
                start = city[v]; // запам'ятовуємо вершину відправки
             visited[v] = true; // помічаємо вершину, як відвідану
            for (int i = 0; i < graph.GetLength(0); i++) // переходимо до всіх вершин, які зв'язані з поточною
            {
                if (graph[v, i] != 0 && visited[i] != true) // пошук зв'язаної не відвіданої вершини
                {
                    int nowkm = km + graph[v, i]; // додаємо відстань до пройденого шляху
                    Console.WriteLine(start + " - " + city[i] + ": " + nowkm + "км");
                    DFS(i, graph, visited, city, nowkm, start); // рекурсія для вершини, яка прив'язана до поточної
                }
            }
        }

    }
}
